package testDAOs;

import DAOs.HikariConnectionPool;
import DAOs.UserDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class TestHikariConnectionPool {
    HikariConnectionPool hk;

    @Before
    public void setUp() {
        hk = new HikariConnectionPool();
    }

    @Test
    public void testConnection(){
        try(Connection conn = hk.getConnection()) {
//            Connection conn = hk.getConnection();
//            conn.close();
        } catch (Exception e) {
            fail();
        }
        assertEquals(true, true);
    }
}
