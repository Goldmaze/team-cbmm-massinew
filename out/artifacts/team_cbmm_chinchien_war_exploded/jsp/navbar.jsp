<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="Articles">
            <img src="../image/i++.png" alt="" height="30px">
        </a>
        <ul class="nav navbar-nav">
            <li class="active"><a href="Articles">Home</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <%String username = (String) session.getAttribute("username");%>
            <c:choose>
                <c:when test="${username != null}">
                    <li>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#newArticleModal">
                            Create New Article
                        </button>
                    </li>
                    <li><a href="HomePage?status=viewMyBlog"><span class="glyphicon glyphicon-log-in"></span> My
                        blog</a></li>
                    <li><a href="UpdateInfoServlet"><span
                            class="glyphicon glyphicon-log-in"></span> My
                        Account</a></li>
                    rt
                    <li><a href="LoginServlet?status=logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    <li><a href="registration.html"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</nav>

<%--when users click the "create" button, direct to this modal--%>
<%@include file="newArticleModal.jsp" %>

