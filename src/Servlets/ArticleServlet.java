package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import SecurityPack.AuthenticationModule;
import org.owasp.encoder.Encode;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;


public class ArticleServlet extends HttpServlet {


//    org.owasp.html.PolicyFactory policy= org.owasp.html.Sanitizers.FORMATTING.and(Sanitizers.IMAGES);
//
//
//    org.owasp.html.PolicyFactory policy= Sanitizers.BLOCKS.and(Sanitizers.FORMATTING).and(Sanitizers.IMAGES);


    org.owasp.html.PolicyFactory supplement = new org.owasp.html.HtmlPolicyBuilder()


            .allowUrlProtocols("data")
            .toFactory();
    //
    PolicyFactory policy = Sanitizers.BLOCKS.and(Sanitizers.IMAGES).and(Sanitizers.STYLES).and(Sanitizers.BLOCKS).and(Sanitizers.LINKS).and(supplement);


//


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        HttpSession myUser= req.getSession();
        displayArticlesList(req, resp);

//if (req.getParameter("newArticle").equals("true")){
//
//}

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {

            String value = cookies[i].getValue();

            if (value.equals("true")) {

//                String trustedTextArea= policy.sanitize(Encode.forJava(req.getParameter("editTextArea")) );
                String trustedTitle = Encode.forJava(req.getParameter("editTitle"));
                String trustedID = Encode.forJava(req.getParameter("editID"));


                req.getSession().setAttribute("textToGive", req.getParameter("editTextArea"));
                req.getSession().setAttribute("titleToGive", trustedTitle);
                req.getSession().setAttribute("iDToGive", trustedID);
                cookies[i].setMaxAge(0);
                HttpSession session = req.getSession();


                modifyArticle(req, resp);


            }

            if (value.equals("newArticle")) {

                String trustedTitle = Encode.forJava(req.getParameter("newTitle"));
                String trustedContent = policy.sanitize(Encode.forHtml(req.getParameter("newContent")));
//               String trustedContent=secondary.sanitize(firstCycle);
//               String t2=policy2.sanitize(trustedContent);
//               String t3=policy3.sanitize(t2);

                req.getSession().setAttribute("newContent",trustedContent);
                req.getSession().setAttribute("newTitle",trustedTitle);

                req.getSession().setAttribute("yearMD",req.getParameter("yearMD"));
                req.getSession().setAttribute("hourMS",req.getParameter("hourMS"));
                cookies[i].setMaxAge(0);
                makeNewArticle(req,resp);
            }

            if (value.equals("delete")){
                String trustedDeleteTitle= Encode.forJava(req.getParameter("articleID"));
                cookies[i].setMaxAge(0);
                deleteArticle( req, resp, trustedDeleteTitle);
            }

// TODO: 29/05/18 delete these cookies after you use them! 
        }


//       makeNewArticle(req,resp);
//        req.getSession().removeAttribute("articles");

        try(ArticleDAO updatedArticles=new ArticleDAO()){
           List<Article> toSupply=updatedArticles.getAllArticles();
           req.getSession().setAttribute("articles",toSupply);

        }catch (Exception e){
            System.out.println("Exception occured here");
        }

        req.getRequestDispatcher("/jsp/mainPage.jsp").forward(req, resp);


    }


    private void modifyArticle(HttpServletRequest req, HttpServletResponse resp) {

        try (ArticleDAO toFind = new ArticleDAO()) {

            HttpSession session = req.getSession();
            String id = (String) session.getAttribute("iDToGive");
            Article toModify = toFind.getArticleByID(id);


            AuthenticationModule toAuthenticate = new AuthenticationModule();
            boolean safeToModify = toAuthenticate.check(session, toModify);
            if (safeToModify) {
                toFind.modifyArticle(session);
            } else {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/noPermission.jsp");
                dispatcher.forward(req, resp);
            }


            // TODO: 24/05/18 tidy this up. It is unacceptable that an article of the same name might be modified when modifing andther article

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }


    }


    private void makeNewArticle(HttpServletRequest req, HttpServletResponse resp) {


        HttpSession session = req.getSession();
        try (ArticleDAO makeNewArticle = new ArticleDAO()) {
            Article article = new Article();

           String newContent=(String) session.getAttribute("newContent");
            String newTitle=(String)session.getAttribute("newTitle");
            String userName=(String)session.getAttribute("username");
            String yearMD = (String)session.getAttribute("yearMD");
            System.out.println("yearMD" + yearMD );
            String hourMS = (String)session.getAttribute("hourMS");
            System.out.println("hourMS" + hourMS );

            article.setTitle(newTitle);
            article.setContent(newContent);
            article.setUsername(userName);
            article.setModifiedDateAndTime(yearMD + " " + hourMS);



            makeNewArticle.createNewArticle(article);

//            req.getRequestDispatcher("/jsp/mainPage.jsp").forward(req, resp);
        }catch (Exception e){
            System.out.println("Exception occured");
        }
    }

    private void displayArticlesList(HttpServletRequest request, HttpServletResponse response) {


        try (ArticleDAO dao = new ArticleDAO()) {

            List<Article> articles = dao.getAllArticles();

            // Adding the article list to the request object
            request.setAttribute("articles", articles);

            try(CommentDAO toUse=new CommentDAO()){
                List<Comment> toTest=toUse.getAllComments();
                request.setAttribute("comments",toTest);
            }
            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);

        }catch (Exception e){
            System.out.println("Exception occured ");
        }
    }


    private void deleteArticle(HttpServletRequest req, HttpServletResponse resp, String id) {
        //delete the comments first


        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database

        try (ArticleDAO toControl = new ArticleDAO()) {

            Article toCheck = toControl.getArticleByID(id);
            AuthenticationModule toAuthenticate = new AuthenticationModule();
            boolean safeToDelete = toAuthenticate.check(req.getSession(), toCheck);
            if (safeToDelete) {

                try(CommentDAO commentDAO = new CommentDAO()){
                    commentDAO.deleteAllComments(id);
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                toControl.deleteArticle(id);
            } else {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/noPermission.jsp");
                dispatcher.forward(req, resp);
            }


            // TODO: 31/05/18 check this tomorrow


        //redirects to main page
//        request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);
//        // TODO: 26/05/18 check whether this code allows articles to be seem after being modified
//        doGet(request,response);


        // TODO: 23/05/18 tidyup this try/catch block 
    } catch (Exception e) {
        System.out.println("Exception occured ");
    }
  

}





}
