package Servlets;

import DAOs.ArticleDAO;
import DAOs.CommentDAO;
import DAOs.User;
import DAOs.UserDAO;
import SecurityPack.AuthenticationModule;
import org.owasp.encoder.Encode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserInfoServlet extends HttpServlet {



    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {

            String value = cookies[i].getValue();
            String userName=(String)req.getSession().getAttribute("username");
            System.out.println("username:"+userName);


             if (value.equals("delete")){

                 System.out.println("found the delete cookie");
                try (UserDAO delete=new UserDAO()){

                    User toCheck=delete.getAllUserInfoByName(userName);

                    AuthenticationModule toAuthenticate= new AuthenticationModule();
                  boolean safeToDelete= toAuthenticate.check(req.getSession(), toCheck);

                  if (safeToDelete) {



                     try (CommentDAO deleteAllComments=new CommentDAO()){
                         deleteAllComments.deleteAllComments(userName);
                         System.out.println("deleted the comments");
                     }

                      try (ArticleDAO deleteAllArticles=new ArticleDAO()){
                          deleteAllArticles.deleteAllArticles(userName);
                          System.out.println("deleted the  articles");
                      }


                      delete.deleteUser(userName);
                      System.out.println("deleted the user");

                      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/mainPage.jsp");
                      dispatcher.forward(req, resp);
                  }
                  else{
                      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/noPermission.jsp");
                      dispatcher.forward(req, resp);
                  }



                }catch (Exception e){
                    System.out.println("An exception occured here");
                }

            }



        }





    }
}
