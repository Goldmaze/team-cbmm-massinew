package DAOs;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

public class Article {
    //from user table
    private int articleId;
    private String title;
    private String content;
    private String modifiedDateAndTime; //todo check the type
    private String status;
    private String username;
    //from topic table
    private String topic;

    public String getModifiedDateAndTime() {
        return modifiedDateAndTime;
    }

    public void setModifiedDateAndTime(String modifiedDateAndTime) {
        this.modifiedDateAndTime = modifiedDateAndTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    @Override
//    public int compare(Object a0,Object a1){
//        System.out.println("QQQQQQQQQQQ");
//        Article article0 = (Article) a0;
//        Article article1 = (Article) a1;
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
////        Date test1 = article0.getModifiedDateAndTime();
////        java.util.Date currentTimeDate=sdf.parse (currentTime);
//        java.util.Date t1 = null;
//        java.util.Date t2 = null;
//        try {
//            t1 = sdf.parse(article0.getModifiedDateAndTime());
//            t2 = sdf.parse(article1.getModifiedDateAndTime());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        int flag = t1.compareTo(t2);
//        return flag;
//    }
//    public int compareTo(Article other) throws ParseException {
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        Date DateAndTime= (Date) sdf.parse (modifiedDateAndTime);
//        Date otherTime = (Date) sdf.parse(other.getModifiedDateAndTime());
//        if (DateAndTime.before(otherTime) ) {
//            return 1;
//        } else {
//            return -1;
//        }
//        if (modifiedDateAndTime > other.getModifiedDateAndTime()) {
//            return -1;
//        } else if (modifiedDateAndTime < other.getModifiedDateAndTime()) {
//            return 1;
//        }

//        Article article = (Article)a;
//        String modifiedDateAndTime = article.getModifiedDateAndTime();
//
//        return this.modifiedDateAndTime.compareTo(modifiedDateAndTime);
    }



